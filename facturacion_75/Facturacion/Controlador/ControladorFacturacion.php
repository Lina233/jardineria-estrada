<?php
session_start();
if(!(isset($_SESSION["NombreUsuario"]))){ //Si la sesión no existe redireccionar al login
    header("Location:../../Index.php");
}

require_once("../../Conexion.php");
require_once('../Modelo/Factura.php'); //Vincular la Clase Factura
require_once('../Modelo/CrudFactura.php'); //Vincular la Clase CrudFactura
require_once('../Modelo/DetalleFactura.php');//Vincular la clase DetalleFactura
require_once('../Modelo/CrudDetalleFactura.php');//Vincular la clase CrudDetalleFactura


$Factura = new Factura(); //Crear el objeto Factura
$CrudFactura = new CrudFactura();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    $Factura->setCodigoCliente($_POST["CodigoCliente"]); //Instanciar el atributo
    $CodigoFacturaGenerado=$CrudFactura::InsertarFactura($Factura); // Llamar el método para Insertar
    $DetalleFactura = new DetalleFactura();
    $CrudDetalleFactura = new CrudDetalleFactura();
    if($CodigoFacturaGenerado>-1)
    {
        $ProductosAgregar = $_POST["ProductosAgregados"];
        $regitroexitoso = 0;
        for($ConsecutivoProducto = 1; $ConsecutivoProducto <= $ProductosAgregar; $ConsecutivoProducto++)
        {
           
            $DetalleFactura->setCodigoFactura($CodigoFacturaGenerado);
            $CodigoProducto = "CodigoProducto".$ConsecutivoProducto;
            if(isset($_POST[$CodigoProducto])) //Si las variables enviadas desde el formulario existen
            {
                $DetalleFactura->setCodigoProducto($_POST[$CodigoProducto]);
                $CantidadProducto = "CantidadProducto".$ConsecutivoProducto;
                $DetalleFactura->setCantidad($_POST[$CantidadProducto]);
                $PrecioProducto = "PrecioProducto".$ConsecutivoProducto;
                $DetalleFactura->setValorUnitario($_POST[$PrecioProducto]);
                $ValorDetalle = "ValorDetalle".$ConsecutivoProducto;
                $DetalleFactura->setValorUnitario($_POST[$CantidadProducto]*$_POST[$PrecioProducto]);
                $regitroexitoso=($CrudDetalleFactura::InsertarDetalleFactura($DetalleFactura));// Llamar el método para Insertar
            }
        }
        if($regitroexitoso==1)
        {
           echo "Registro de Factura Exitoso";
        } 
        else
        {
            echo "Problemas en el registro"; 
        }
            
    }
    else
    {
        echo "Problemas en la inserción";
    }
}

?>