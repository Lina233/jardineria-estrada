<?php
session_start();
if(!(isset($_SESSION["NombreUsuario"]))){ //Si la sesión no existe redireccionar al login
    header("Location:../../Index.php");
}
require_once("../../Conexion.php");
//require_once('../../Cliente/Modelo/Cliente.php');//Vincular la clase Cliente
//require_once('../../Cliente/Modelo/CrudCliente.php');//Vincular la clase CrudCliente
//$Cliente = new Cliente(); //Crear el objeto Cliente
//$CrudCliente = new CrudCliente();
//$ListaClientes = $CrudCliente->ListarClientes(); //Llamado al método ListarCompetencia
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <h1 align="center">Facturación</h1>
    <form action="../Controlador/ControladorFacturacion.php" method="POST">
        Cliente:
        <select name="CodigoCliente" id="CodigoCliente">
            <option option="">El que</option>
            <option value="20">lo</option>
            <option value="21">Lea es gay</option>
        </select>
        <br>
        Producto:
        <select name="CodigoProducto" id="CodigoProducto">
            <option option="">Seleccione</option>
            <option value="10">Arroz</option>
            <option value="11">Panela</option>
        </select>
        <br>
        Precio:
        <input type="text" id="PrecioProducto" name="PrecioProducto">
        <br>
        Cantidad:
        <input type="text" id="CantidadProducto" name="CantidadProducto">
        <br>
        <button type="button" name="AgregarProducto" id="AgregarProducto" onclick="AgregarDetalle()">Agregar</button>
        <br>
        <input type="hidden" name="Registrar" id="Registrar">

        <button type="submit">Registrar</button>
        <br>
        <table id="ListaProductos" border="1">
        <thead>
        <th>Nombre</th>
        <th>Codigo</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th>Valor Detalle</th>
        <th></th>
        </thead>
        <tbody>

        </tbody>
        </table>
        <input type="text" id="ProductosAgregados" name="ProductosAgregados" value="0">
    </form>
</body>
<script>
    function AgregarDetalle()
    {
        let CodigoProducto = $('#CodigoProducto').val();
        let NombreProducto = $('select[name="CodigoProducto"] option:selected').text();
        let CantidadProducto = $('#CantidadProducto').val();
        let PrecioProducto = $('#PrecioProducto').val();
        let ValorDetalle = CantidadProducto*PrecioProducto;

        $('#ProductosAgregados').val(parseInt($('#ProductosAgregados').val()) + 1);
        let ConsecutivoProducto = $('#ProductosAgregados').val();

        let htmlTags = '<tr id="'+ConsecutivoProducto+'" >' +
        '<td>' + NombreProducto + '</td>' +
        '<td>' + '<input type="text" id="CodigoProducto'+ConsecutivoProducto+'" name="CodigoProducto'+ConsecutivoProducto+'" value="'+CodigoProducto+'">'+'</td>' +
        '<td>' + '<input type="text" id="CantidadProducto'+ConsecutivoProducto+'" name="CantidadProducto'+ConsecutivoProducto+'" value="'+CantidadProducto+'">'+'</td>' +
        '<td>' + '<input type="text" id="PrecioProducto'+ConsecutivoProducto+'" name="PrecioProducto'+ConsecutivoProducto+'" value="'+PrecioProducto+'">'+'</td>' +
        '<td>' + '<input type="text" id="ValorDetalle'+ConsecutivoProducto+'" name="ValorDetalle'+ConsecutivoProducto+'" value="'+ValorDetalle+'">'+'</td>' +
        '<td>' +  '<button class="borrar" type="button" onclick="EliminarDetalle('+ConsecutivoProducto+')" >Eliminar</button>' +'</td>' +
        '</tr>';
       $('#ListaProductos tbody').append(htmlTags);
    }

    function EliminarDetalle(ConsecutivoProducto)
    {
        alert(ConsecutivoProducto);
    }

    $(function () {
        $(document).on('click', '.borrar', function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
        });
    });

</script>
</html>