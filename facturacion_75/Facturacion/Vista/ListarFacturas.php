<?php
session_start();
if(!(isset($_SESSION["NombreUsuario"]))){ //Si la sesión no existe redireccionar al login
    header("Location:../../Index.php");
}
require_once('../../Conexion.php'); 
require_once('../Modelo/Factura.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/DetalleFactura.php');
require_once('../Modelo/CrudDetalleFactura.php');

$CrudDetalleFactura = new CrudDetalleFactura(); //Crear de un objeto CrudCompetencia
$ListaFacturas = $CrudDetalleFactura->ListarFacturas(); //Llamado al método ListarCompetencia

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1 align="center">Listado de Facturaas</h1>
    <a href="../TCPDF/examples/reportepdfcompetencias.php">Reporte Pdf</a>
    <table align="center" border="1">
        <thead>
        <tr>
            <th>Código Factura</th>
            <th>Código Detalle</th>
            <th>Código Producto</th>
            <th>Cantidad</th>
            <th>Valor Unitario</th>
            <th>Valor Detalle</th>
            <th>Acciones</th>
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaFacturas as $Factura){
                ?>
                <tr>
                    <td><?php echo $Factura->getCodigoFactura(); ?></td>
                    <td><?php echo $Factura->getCodigoDetalleFactura(); ?></td>
                    <td><?php echo $Factura->getCodigoProducto(); ?></td>
                    <td><?php echo $Factura->getCantidad(); ?></td>
                    <td><?php echo $Factura->getValorUnitario(); ?></td>
                    <td><?php echo $Factura->getCantidad()*$Factura->getValorUnitario(); ?></td>
                    <td>
                    <a href="EditarFacturas.php?CodigoFactura=<?php echo $Factura->getCodigoFactura(); ?>">Editar</a> 
                    <a href="../Controlador/ControladorFactura.php?CodigoFactura=<?php echo $Factura->getCodigoFactura(); ?>&Accion=EliminarCompetencia">Eliminar</a></td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
</body>
</html>