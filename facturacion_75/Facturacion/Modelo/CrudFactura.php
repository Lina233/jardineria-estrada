<?php
class CrudFactura{
    public function __construct(){
    
    }

    public function InsertarFactura($Factura){
        $CodigoFacturaGenerado = -1;
        $Db = Db::Conectar();
        $Sql =$Db->prepare('INSERT INTO  facturas(CodigoFactura,CodigoCliente,FechaFactura)
        VALUES(NULL,:CodigoCliente,NOW())');
        $Sql->bindValue('CodigoCliente',$Factura->getCodigoCliente());
        try{
            $Sql->execute();
            $CodigoFacturaGenerado = $Db->lastInsertId();//Consultar el último Id insertado para el usurio que está conectado a la DB
        }
        catch(Exception $e){
            echo $e->getMessage();
            //die();
        }
        return $CodigoFacturaGenerado;
    }
}

?>