<?php
class CrudDetalleFactura{
    public function __construct(){
    
    }

    public function InsertarDetalleFactura($DetalleFactura){
        $detalleinsertado = 0;
        $Db = Db::Conectar();
        $Sql =$Db->prepare('INSERT INTO  detallefacturas(CodigoDetalleFactura, CodigoFactura,CodigoProducto,Cantidad,ValorUnitario)
        VALUES(NULL, :CodigoFactura,:CodigoProducto,:Cantidad,:ValorUnitario)');
        $Sql->bindValue('CodigoFactura',$DetalleFactura->getCodigoFactura());
        $Sql->bindValue('CodigoProducto',$DetalleFactura->getCodigoProducto());
        $Sql->bindValue('Cantidad',$DetalleFactura->getCantidad());
        $Sql->bindValue('ValorUnitario',$DetalleFactura->getValorUnitario());
        try{
            $Sql->execute();
            $detalleinsertado = 1;
        }
        catch(Exception $e){
            echo $e->getMessage();
            //die();
        }
        return $detalleinsertado;
    }

    public function ListarFacturas(){
        $Db = Db::Conectar();
        $ListarFacturas = [];
        $Sql = $Db->query('SELECT facturas.CodigoCliente,facturas.FechaFactura,detalleFacturas.* FROM detallefacturas 
                           INNER JOIN facturas 
                           ON detallefacturas.CodigoFactura = facturas.CodigoFactura');
        $Sql->execute();
        foreach($Sql->fetchAll() as $Factura){
            $MiFactura = new Factura();
            $MiDetalleFactura = new DetalleFactura();
            $MiFactura->setCodigoCliente($Factura['CodigoCliente']);
            $MiFactura->setFechaFactura($Factura['FechaFactura']);
            $MiDetalleFactura->setCodigoFactura($Factura['CodigoFactura']);
            $MiDetalleFactura->setCodigoDetalleFactura($Factura['CodigoDetalleFactura']);
            $MiDetalleFactura->setCodigoProducto($Factura['CodigoProducto']);
            $MiDetalleFactura->setCantidad($Factura['Cantidad']);
            $MiDetalleFactura->setValorUnitario($Factura['ValorUnitario']);
            $ListaFacturas[] = $MiDetalleFactura;
        }
        return $ListaFacturas;
    }

}

?>