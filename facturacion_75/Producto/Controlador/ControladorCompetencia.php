<?php
session_start();
if(!(isset($_SESSION["NombreUsuario"]))){ //Si la sesión no existe redireccionar al login
    header("Location:../../Index.php");
}

require_once("../../Conexion.php");
require_once('../Modelo/Competencia.php'); //Vincular la Clase Competencia
require_once('../Modelo/CrudCompetencia.php'); //Vincular la Clase Crud

$Competencia = new Competencia(); //Crear el objeto Competencia
$CrudCompetencia = new CrudCompetencia();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    echo "Registrar";
    $Competencia->setCodigoCompetencia($_POST["CodigoCompetencia"]); //Instanciar el atributo
    $Competencia->setNombreCompetencia($_POST["NombreCompetencia"]); //Instanciar el atributo
    echo $Competencia->getNombreCompetencia(); //Verificar instanciación
    $CrudCompetencia::InsertarCompetencia($Competencia); // Llamar el método para Insertar
}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    echo "Modificar";
    $Competencia->setCodigoCompetencia($_POST["CodigoCompetencia"]); //Instanciar el atributo
    $Competencia->setNombreCompetencia($_POST["NombreCompetencia"]); //Instanciar el atributo
    echo $Competencia->getNombreCompetencia(); //Verificar instanciación
    $CrudCompetencia::ModificarCompetencia($Competencia); // Llamar el método para Modificar
}
elseif($_GET["Accion"]=="EliminarCompetencia"){
    $CrudCompetencia::EliminarCompetencia($_GET["CodigoCompetencia"]); // Llamar el método para Modificar
}

?>