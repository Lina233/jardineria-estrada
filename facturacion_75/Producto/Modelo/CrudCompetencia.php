<?php
    //require_once('../Conexion.php');
    class CrudCompetencia{
    
        public function __construct(){
        }

        public function InsertarCompetencia($Competencia){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Insert = $Db->prepare('INSERT INTO competencias VALUES(:CodigoCompetencia,:NombreCompetencia)'); 
            $Insert->bindValue('CodigoCompetencia',$Competencia->getCodigoCompetencia());
            $Insert->bindValue('NombreCompetencia',md5($Competencia->getNombreCompetencia()));
            try{
                $Insert->execute(); //Ejecutar el Insert
                echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        public function ModificarCompetencia($Competencia){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE competencias SET NombreCompetencia=:NombreCompetencia
            WHERE CodigoCompetencia=:CodigoCompetencia'); 
            $Sql->bindValue('CodigoCompetencia',$Competencia->getCodigoCompetencia());
            $Sql->bindValue('NombreCompetencia',$Competencia->getNombreCompetencia());
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }


        public function EliminarCompetencia($CodigoCompetencia){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM competencias WHERE CodigoCompetencia=:CodigoCompetencia'); 
            $Sql->bindValue('CodigoCompetencia',$CodigoCompetencia);
            $CodigoCompetencia = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        //Listar todos los registros de la tabla
        public function ListarCompetencias(){
            $Db = Db::Conectar();
            $ListaCompetencias = [];
            $Sql = $Db->query('SELECT * FROM competencias');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Competencia){
                $MyCompetencia = new Competencia();
                //echo $Competencia['CodigoCompetencia']."----".$Competencia['NombreCompetencia'];
                $MyCompetencia->setCodigoCompetencia($Competencia['CodigoCompetencia']);
                $MyCompetencia->setNombreCompetencia($Competencia['NombreCompetencia']);
                $ListaCompetencias[] = $MyCompetencia;
            }
            return $ListaCompetencias;
        }

        public function ObtenerCompetencia($CodigoCompetencia){ //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM competencias WHERE CodigoCompetencia=:CodigoCompetencia');
            $Sql->bindValue('CodigoCompetencia',$CodigoCompetencia);
            $MyCompetencia = new Competencia();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Competencia = $Sql->fetch(); //Se almacena en la variable $Competencia los datos de la variable $Sql
                $MyCompetencia->setCodigoCompetencia($Competencia['CodigoCompetencia']);
                $MyCompetencia->setNombreCompetencia($Competencia['NombreCompetencia']);

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyCompetencia;
        }

    }


//$Crud = new CrudCompetencia();
//$Crud->ListarCompetencias();


?>