<?php
    class Competencia{
        //Parámentros de entrada
        private $CodigoCompetencia;
        private $NombreCompetencia;

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setCodigoCompetencia($CodigoCompetencia){
            $this->CodigoCompetencia = $CodigoCompetencia;
        }

        public function getCodigoCompetencia(){
            return $this->CodigoCompetencia;
        }

        public function setNombreCompetencia($NombreCompetencia){
            $this->NombreCompetencia = $NombreCompetencia;
        }

        public function getNombreCompetencia(){
            return $this->NombreCompetencia;
        }
    }
    
    //Testear funcionalidad de clase.
    /*
    $Competencia = new Competencia(); //Crear objeto
    $Competencia->setCodigoCompetencia(27);
    $Competencia->setNombreCompetencia('Python');
    echo "Código Competencia: ".$Competencia->getCodigoCompetencia().
    " NombreCompetencia: ".$Competencia->getNombreCompetencia();
    */
?>