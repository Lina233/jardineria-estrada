<?php
require_once("../../Conexion.php");
require_once("../Modelo/Usuario.php"); //Incluir el modelo Usuario
require_once("../Modelo/CrudUsuario.php");


$Usuario = new Usuario(); //Crear un objeto vacio de la clase Usuario
$CrudUsuario = new CrudUsuario();

if(isset($_POST["Acceder"])){ //Validar que se realizó la petición de Acceder
    $Usuario->setNombreUsuario($_POST["NombreUsuario"]); //Asignar valor a atributo NombreUsuario
    $Usuario->setContrasena($_POST["Contrasena"]); //Asignar valor a atributo Contraseña
    //var_dump($Usuario);
    $Usuario = $CrudUsuario->ValidarAcceso($Usuario);
    //var_dump($Usuario);
    if($Usuario->getExiste() == 1){
        session_start(); //Inicializar sesiones
        //Definir las variables de sesión a emplear en el aplicativo
        $_SESSION["NombreUsuario"] = $Usuario->getNombreUsuario();
        $_SESSION["IdUsuario"] = $Usuario->getIdUsuario();
        $_SESSION["IdRol"] = $Usuario->getIdRol();
        header("Location:../../Menu.php");
    }
    else
    {
        ?>
        <script>
            alert("Usuario y/o clave incorrectos");
            document.location.href = "../../Index.php";
        </script>
    <?php
    }
}
else //En caso contrario envío al Login
{
    header("Location:../../Index.php");
}

?>